import 'package:fibonacci/fibonacci.dart' as fibonacci;

void main(List<String> arguments) {
  if (arguments.length == 1) {
    var n = int.parse(arguments[0]);
    print('Hello world: ${fibonacci.calculate(n)}!');
  } else {
    print('No arguments');
  }
}
