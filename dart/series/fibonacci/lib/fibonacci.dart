int calculate(int x) {
    if (x < 2) {
        return x;
    }
    return calculate(x - 1) + calculate(x - 2);
}