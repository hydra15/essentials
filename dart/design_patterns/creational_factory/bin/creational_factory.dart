import 'package:creational_factory/factory/creator.dart' as creational_factory;

void main(List<String> arguments) {
  print('Hello world: ${creational_factory.calculate()}!');
}
