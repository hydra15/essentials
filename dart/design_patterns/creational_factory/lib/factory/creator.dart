import 'package:creational_factory/interface/i_bottle.dart';

abstract class Creator {
  IBottle createBottle();

  void make() {
    IBottle bottle = createBottle();
    bottle.sizeBottle();
    bottle.makeBottle();
    bottle.fillBottle();
  }
}
