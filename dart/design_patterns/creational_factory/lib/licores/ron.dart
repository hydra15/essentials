import 'package:creational_factory/interface/i_bottle.dart';

class Ron implements IBottle {
  @override
  void fillBottle() {
    print('ron - fillBottle');
  }

  @override
  void makeBottle() {
    print('ron - makeBottle');
  }

  @override
  void sizeBottle() {
    print('ron - sizeBottle');
  }
}
