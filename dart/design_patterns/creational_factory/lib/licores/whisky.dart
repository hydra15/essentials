import 'package:creational_factory/interface/i_bottle.dart';

class Whisky implements IBottle {
  @override
  void fillBottle() {
    print('whisky - fillBottle');
  }

  @override
  void makeBottle() {
    print('whisky - makeBottle');
  }

  @override
  void sizeBottle() {
    print('whisky - sizeBottle');
  }
}
