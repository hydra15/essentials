abstract class IBottle {
  void sizeBottle();
  void makeBottle();
  void fillBottle();
}
